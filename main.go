package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"unicode"
)

var (
	notified = make(map[string]map[string]bool)
)

// notify maybe sends a notification (if we haven't sent it before)
//
// This makes sure we only notify about a particular release once, even though
// it probably contains multiple files
func notify(user, item string) error {
	_, ok := notified[user]
	// There is no user release map yet - make it
	if !ok {
		notified[user] = make(map[string]bool)
	}

	// Check if we've already notified the user. Only proceed if we haven't.
	_, ok = notified[user][item]
	if !ok {
		msg := fmt.Sprintf("%s ⬇ %s", user, item)
		fmt.Println(msg)
		// Then mark the release as notified about
		notified[user][item] = true
	}

	return nil
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	var r rune
	for scanner.Scan() {
		line := scanner.Text()
		split := strings.Split(line, " ")
		user := split[2]
		path := split[6]
		split = strings.Split(path, "/")
		item := ""

		// Determine which item is being downloaded. This finds the first
		// directory that starts with an uppercase character. Simple, but probably
		// works most of the time.
		for _, s := range split {
			for _, c := range s {
				r = c
				if unicode.IsUpper(r) {
					item = s
					break
				}
				break
			}
			if item != "" {
				break
			}
		}

		err := notify(user, item)
		if err != nil {
			fmt.Println(err)
		}
	}

	if scanner.Err() != nil {
		fmt.Println(scanner.Err())
	}
}
